import React, {useEffect, useState} from 'react';
import './App.css';
import {Card, CardContent, FormControl, MenuItem, Select} from '@material-ui/core'
import InfoBox from "./InfoBox";
import Map from "./Map"
import Table from "./Table"
import {prettyPrintStat, prettyPrintTotal, sortData} from "./util";
import LineGraph from "./LineGraph";
import "leaflet/dist/leaflet.css"


function App() {

    const [countries, setCountries] = useState([]);
    const [countryName, setCountryName] = useState('worldwide');
    const [country, setCountry] = useState('worldwide');
    const [countryInfo, setCountryInfo] = useState({});
    const [tableData, setTableData] = useState([]);
    const [mapCenter, setMapCenter] = useState({lat: 34.80746, lng: -40.4796});
    const [mapZoom, setMapZoom] = useState(3);
    const [mapCountries, setMapCountries] = useState([]);
    const [typesOfCases, setCasesType] = useState("cases");

    useEffect(() => {
        fetch("https://disease.sh/v3/covid-19/all")
            .then((response) => response.json())
            .then((data) => setCountryInfo(data))
    }, [])

    useEffect(() => {

        const getCountriesData = async () => {
            await fetch("https://disease.sh/v3/covid-19/countries")
                .then((response) => response.json())
                .then((data) => {
                    const countries = data.map((country) => (
                        {
                            name: country.country,
                            value: country.countryInfo.iso2,
                        }
                    ));
                    const sortedData = sortData(data);
                    setTableData(sortedData);
                    setCountries(countries);
                    setMapCountries(data);
                });
        };
        getCountriesData();
    }, [])

    const onCountryChange = async (event) => {
        const countryCode = event.target.value;

        const url = countryCode === 'worldwide' ? "https://disease.sh/v3/covid-19/all" : `https://disease.sh/v3/covid-19/countries/${countryCode}`;
        await fetch(url)
            .then(response => response.json())
            .then((data) => {
                setCountry(countryCode);
                setCountryInfo(data);
                setCountryName(data.country === undefined ? 'worldwide' : data.country);
                let latitude, longitude, zoom = 0;
                if (countryCode === 'worldwide') {
                    latitude = 34.80746;
                    longitude = -40.4796;
                    zoom = 3
                } else {
                    latitude = data.countryInfo.lat;
                    longitude = data.countryInfo.long;
                    zoom = 4
                }
                setMapCenter({lat: latitude, lng: longitude})
                setMapZoom(zoom)
            });

    }
    const isWorldWide = country === 'worldwide'

    return (
        <div className="App">

            <div className="app__left">
                <div className="app__header">
                    <h1>COVID-19 TRACKER</h1>

                    <FormControl className="app__dropdown">

                        <Select variant="outlined" onChange={onCountryChange} value={country}>
                            <MenuItem value="worldwide">Worldwide</MenuItem>
                            {
                                countries.map(country => (
                                    <MenuItem value={country.value}>{country.name}</MenuItem>
                                ))
                            }
                        </Select>
                    </FormControl>
                </div>

                <div className="app__stats">
                    <InfoBox onClick={(e) => setCasesType('cases')}
                             active={typesOfCases === 'cases'}
                             currentColor="orange"
                             title="Coronavirus Cases"
                             total={prettyPrintTotal(countryInfo.cases)}
                             cases={prettyPrintStat(countryInfo.todayCases)}/>
                    <InfoBox onClick={(e) => setCasesType('recovered')}
                             active={typesOfCases === 'recovered'}
                             currentColor="green"
                             title="Recovered"
                             total={prettyPrintTotal(countryInfo.recovered)}
                             cases={prettyPrintStat(countryInfo.todayRecovered)}/>
                    <InfoBox onClick={(e) => setCasesType('deaths')}
                             active={typesOfCases === 'deaths'}
                             currentColor="red"
                             title="Deaths"
                             total={prettyPrintTotal(countryInfo.deaths)}
                             cases={prettyPrintStat(countryInfo.todayDeaths)}/>
                </div>
                <Map casesType={typesOfCases} countries={mapCountries} center={mapCenter} zoom={mapZoom}/>
            </div>

            <Card className="app__right">
                <CardContent>

                    <h3>Live cases by Country</h3>
                    <Table countries={tableData}/>
                    <h3>{typesOfCases} {isWorldWide ? '' : 'in'} {countryName}</h3>
                    <LineGraph className="app__graph" caseType={typesOfCases}
                               country={isWorldWide ? 'all' : country}/>
                </CardContent>

            </Card>
        </div>
    );
}

export default App;
