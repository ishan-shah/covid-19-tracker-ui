import React from 'react'
import {Card, CardContent, Typography} from "@material-ui/core";
import './InfoBox.css'
function InfoBox({title, cases, active, currentColor, total, ...props}) {

    return (
        <Card onClick={props.onClick} className={`infoBox ${active && 'infoBox--selected'} infoBox--${currentColor}`}>

            <CardContent>
                <Typography className="infoBox__title" color="textSecondary">
                    {title}
                </Typography>
                <h2 className={`infoBox__cases infoBox--${currentColor}`}>
                    {cases}
                </h2>
                <Typography className="infoBox__total" color="textSecondary">
                    {total} Total
                </Typography>
            </CardContent>
        </Card>
    )
}

export default InfoBox