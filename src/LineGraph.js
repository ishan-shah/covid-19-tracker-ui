import React, {useEffect, useState} from "react";
import {Line} from "react-chartjs-2";
import numeral from "numeral"
import {casesTypeColors} from "./util"

const options = {
    legend: {
        display: false,
    },
    elements: {
        point: {
            radius: 1.2,
        }
    },
    maintainAspectRatio: false,
    tooltip: {
        modes: "index",
        intersect: false,
        callbacks: {
            label: function (tooltipItem, data) {
                return numeral(tooltipItem.value).format("+0.0");
            }
        }
    },
    scales: {
        xAxes: [
            {
                type: "time",
                time: {
                    format: "MM/DD/YY",
                    tooltipFormat: "ll"
                }
            }
        ],
        yAxes: [
            {
                gridLines: {
                    display: false
                },
                ticks: {
                    callback: function (value, index, values) {
                        return numeral(value).format("0a")
                    }
                }
            }
        ]
    }
}

const buildChartData = (data, caseType) => {
    let chartData = [];
    let lastDataPoint;
    for (let date in data.cases) {
        if (lastDataPoint) {
            //console.log("more:", data[caseType][date] - lastDataPoint);
            let newDataPoint = {
                x: date,
                y: data[caseType][date] - lastDataPoint
            }
            chartData.push(newDataPoint);
        }
        //lastDataPoint = data['cases'][date];
        lastDataPoint = data[caseType][date];
    }
    return chartData;
}

function LineGraph({caseType = 'cases', country = 'all', ...props}) {

    const [data, setData] = useState({})
    useEffect(() => {

        const fetchData = async () => {
            await fetch("https://disease.sh/v3/covid-19/historical/" + country + "?lastdays=120")
                .then(response => response.json())
                .then((data) => {
                    const chartData = buildChartData(country === 'all' ? data : data.timeline, caseType);
                    setData(chartData);
                }).catch(e => {
                    let dummyData = {
                        "cases": {}
                    };
                    const chartData = buildChartData(dummyData, caseType);
                    setData(chartData);
                })
        }
        fetchData();
    }, [caseType, country])

    return (
        <div className={props.className}>
            {data?.length > 0 && (
                <Line
                    options={options}
                    data={{
                        datasets: [{
                            backgroundColor: casesTypeColors[caseType].half_op,
                            borderColor: casesTypeColors[caseType].hex,
                            fillOpacity: 0.3,
                            data: data
                        }]
                    }}/>
            )}

        </div>
    );
}

export default LineGraph